HyperSwitch
===========

A utility for quickly switching between open applications using the keyboard.

Applications are selected by searching within titles.

HyperSwitch uses the EWMH library to list and manipulate the active windows.

PyGTK
-----

HyperSwitch requires PyGTK to run. To support installation in a virtual
environment, PyGTK has not been listed as a dependency in the setup.py
configuration.

PyGTK is usually included in a standard python installation. To make PyGTK
available in a virtual environment refer to
https://stackoverflow.com/questions/9064289/installing-pygtk-in-virtualenv.
